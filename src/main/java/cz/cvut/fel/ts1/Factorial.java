package cz.cvut.fel.ts1;

public class Factorial {
    public long factorial(int n) {
        long res = 1;
        for (int i = n; i > 1; i--) res *= i;
        return res;
    }
}
