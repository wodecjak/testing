package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorialTest {
    @Test
    void factorialTest() {
        Factorial newfactorial = new Factorial();
        long result = newfactorial.factorial(4);
        assertEquals(24, result);
    }

}
